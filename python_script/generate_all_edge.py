__author__ = 'andompesta'
import random
path = '/home/andompesta/Dropbox/Project DS'

sources_dict = {}
edges_weight = {}
with open(path + '/dataset3/ca-HepTh.txt', 'r') as reader:
    for line in reader:
        line_ids = line[:-1].split('\t')
        source_node = int(line_ids[0])
        target_node = int(line_ids[1])

        if target_node in sources_dict:
            sources_dict[target_node].append(source_node)
        else:
            sources_dict[target_node] = [source_node]


for target_node in sources_dict:
    incoming_overall_weight = 0
    for source_node in sources_dict[target_node]:
        edge_weight = random.random()
        incoming_overall_weight += edge_weight
        edges_weight[(source_node, target_node)] = edge_weight
    for source_node in sources_dict[target_node]:
        edges_weight[(source_node, target_node)] = edges_weight[(source_node, target_node)]/incoming_overall_weight


with open(path + '/dataset3/edges_weight.txt', 'w') as writer:
    for edge in edges_weight:
        writer.write(str(edge[0]) + '\t' + str(edge[1]) + '\t'+ str(edges_weight[edge])[:10] + '\n')
