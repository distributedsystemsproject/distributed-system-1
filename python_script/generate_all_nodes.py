__author__ = 'ando'
import random
path = '/home/andompesta/Dropbox/Project DS'
ids_dictionary = set()

with open(path + '/dataset3/ca-HepTh.txt', 'r') as reader:
    for line in reader:
        line_ids = line[:-1].split('\t')
        ids_dictionary.add(int(line_ids[0]))
        ids_dictionary.add(int(line_ids[1]))

with open(path + '/dataset3/ids_with_opinion_threshold.txt', 'w') as writer:
    for id in ids_dictionary:

        opinion = random.uniform(-1, 1.1)
        if opinion > 1:
            opinion = 1
        threshold = str(random.random())[:10]
        writer.write(str(id) +" " + str(opinion)[:10] + " " + threshold + '\n')
