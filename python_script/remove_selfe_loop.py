__author__ = 'andompesta'

path = '/Users/ando/Dropbox/Project DS'

class Edge:
    def __init__(self, source_node, target_node, weight):
        self.source_node = source_node
        self.target_node = target_node
        self.weight = weight

    def add_weight(self, weight):
        self.weight = weight

    def to_string(self):
        return str(self.source_node) + "\t" + str(self.target_node) + "\t" + str(self.weight)

edges = []
with open(path + '/dataset2/edges_weighted.txt', 'r') as reader:
    for line in reader:
        tokens = line[:-1].split('\t')
        source_node = int(float(tokens[0]))
        target_node = int(float(tokens[1]))
        weight = float(tokens[2])

        if not source_node == target_node:
            edges.append(Edge(source_node, target_node, weight))
        else:
            print("%d %d %f" % (source_node, target_node, weight))

with open(path + '/dataset2/edges_weighted_1.txt', 'w') as writer:
    for edge in edges:
        writer.write(edge.to_string() + '\n')