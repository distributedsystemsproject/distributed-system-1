import DAO.MyEdge;
import DAO.MyNode;
import Utils.RandomUtils;
import Utils.Read;
import Utils.Write;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.io.File;
import java.io.FileReader;
import java.util.*;

/**
 * Created by ando on 10/9/15.
 */
public class AddWeightEdge {
    private static Properties prop = new Properties();


    public static void main(String[] args) throws Exception {
        try {
            prop.load(new FileReader("config.properties"));
        }catch (Exception e){
            throw e;
        }

        //Read nodes
        Map<Double, MyNode> node_list = Read.readAllNodeWithOpinion(prop.getProperty("BASE_DIR") + prop.getProperty("DATA_2") + File.separator + "ids_with_opinion.txt");
        Map<Double, List<Double>> node_edges = Read.readAllEdge(prop.getProperty("BASE_DIR") + prop.getProperty("DATA_2") + File.separator + "twitter_combined.txt");
        Map<Double, List<MyEdge>> edges_weights = new HashMap<Double, List<MyEdge>>();



        SimpleDirectedWeightedGraph<MyNode, DefaultWeightedEdge> directedGraph = new SimpleDirectedWeightedGraph<MyNode, DefaultWeightedEdge>(DefaultWeightedEdge.class);

        //Add all node to the graph
        for (MyNode node_id : node_list.values()){
            directedGraph.addVertex(node_id);
        }

        //Add all edges to the graph
        for (double key_node : node_edges.keySet()){
            MyNode node_a = node_list.get(key_node);
            List<Double> key_node_edges =  node_edges.get(key_node);
            for (double node_edge : key_node_edges ){
                MyNode node_b =  node_list.get(node_edge);
                try {
                    directedGraph.addEdge(node_a, node_b);
                }catch (IllegalArgumentException ie){
                    System.out.println(node_a.toString() + " " + node_b.toString());
                    throw ie;
                }
            }
        }

        for (double node_id : node_list.keySet()){
            MyNode node = node_list.get(node_id);

            if (edges_weights.containsKey(node.getNode_id())){
                throw new Exception("Error node id duplicated");
            }
            List<MyEdge> node_source_edges = new ArrayList<MyEdge>();


            double sum_weight = 0D;
            Set<DefaultWeightedEdge> in_edges = directedGraph.incomingEdgesOf(node);

            //set random weight
            for (DefaultWeightedEdge edge : in_edges){
                double edge_weight = RandomUtils.edgeWeightGenerator();
                sum_weight += edge_weight;
                directedGraph.setEdgeWeight(edge, edge_weight);
            }

            //normalize edge weight to 1
            for (DefaultWeightedEdge edge : in_edges){
                double edge_weight = directedGraph.getEdgeWeight(edge);
                directedGraph.setEdgeWeight(edge, (edge_weight/ sum_weight));

                //saving edge
                MyNode source_node = directedGraph.getEdgeSource(edge);
                node_source_edges.add(new MyEdge(source_node.getNode_id(), (edge_weight/ sum_weight)));
            }
            //saving edges
            edges_weights.put(node.getNode_id(), node_source_edges);
        }

        Write.writeEdgesWeight(prop.getProperty("BASE_DIR") + prop.getProperty("DATA_2") + File.separator + "edges_weighted.txt", edges_weights);

    }
}
