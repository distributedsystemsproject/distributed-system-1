import Algorithem.MyBFS;
import Algorithem.InizializeGraph;
import Algorithem.PageRank;
import DAO.*;
import Utils.Read;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

/**
 * Created by ando on 10/19/15.
 */


public class MainPageRank  {
    private static Properties prop = new Properties();
    private static SimpleDirectedWeightedGraph directedGraph;
    private static List<MyNode> seed_node;
    private static GraphPerformance performance;

    private static int K = 200;
    private static String DATA_SET = "DATA_3";
    private static String log_name = "pr-k="+K+".log";
    private static BufferedWriter LOG;

    public static void main(String[] args) throws Exception {
        try {
            prop.load(new FileReader("config.properties"));

            seed_node = new ArrayList<>();
            performance = new GraphPerformance();


            //Read nodes
            Map<Double, MyNode> node_list = Read.readAllNodeWithOpinion(prop.getProperty("BASE_DIR") + File.separator + prop.getProperty(DATA_SET) + File.separator + "ids_with_opinion_threshold.txt");
            //Read edges
            Map<Double, List<MyEdge>> edges_list = Read.readAllEdgeWithInterest(prop.getProperty("BASE_DIR") + File.separator + prop.getProperty(DATA_SET) + File.separator + "edges_weighted.txt");


            directedGraph = InizializeGraph.init(node_list, edges_list);
            LOG = new BufferedWriter(new FileWriter(prop.getProperty("BASE_DIR") + File.separator + prop.getProperty(DATA_SET) + File.separator + log_name));

            List<PageRank.PRScore> PR_seed_nodes = new ArrayList<>(PageRank.calculatePageRank(0.85, directedGraph));

            Collections.sort(PR_seed_nodes, new Comparator<PageRank.PRScore>() {
                @Override
                public int compare(PageRank.PRScore o1, PageRank.PRScore o2) {
                    return Double.compare(o2.pr_score, o1.pr_score);
                }
            });

            PR_seed_nodes = PR_seed_nodes.subList(0, K);

            for (int i = 0; i < PR_seed_nodes.size(); i++) {
                PageRank.PRScore node_score = PR_seed_nodes.get(i);
                System.out.println("\n\nturn: " + node_score.id + "\n\n");

                MyNode node = node_list.get(node_score.id);

                Map<Double, MyNodeUpdateInfo> nodes_to_activate = new HashMap<>();
                Gains gain = MyBFS.marginalGain(node, directedGraph, nodes_to_activate);
                MyBFS.activateNode(nodes_to_activate, node_list);
                //System.err.println(candicate_nodes.size() + "\t" + best_candiate.getNode_id());
                performance.total_opinion += gain.opinion_gain;
                performance.positive_activated_nodes += gain.positively_activated_gain;
                LOG.write(i + "\t" + node.getNode_id() + "\t" + performance.total_opinion + "\t" + performance.positive_activated_nodes + "\t" +gain.opinion_gain + "\t" + gain.positively_activated_gain + "\n");
            }

          /*  long start = System.nanoTime();
            long end = System.nanoTime();
            long time = end - start;
            LOG.write(Long.toString(time)+"\n");*/
            System.out.println("FINISH");

        }catch (Exception e){
            throw e;
        }finally {
            if (LOG != null){
                LOG.close();
            }
        }
    }
}
