import Algorithem.CELF;
import Algorithem.MyBFS;
import DAO.Gains;
import DAO.MyEdge;
import DAO.MyNode;
import DAO.MyNodeUpdateInfo;
import Utils.Read;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.io.File;
import java.io.FileReader;
import java.util.*;

public class Main {

    private static Properties prop = new Properties();
    private static List<MyNode> seed_node;
    private static final int K = 50;
    private static final int p = 4;
    private static int activated_nodes = 0;
    private static int positive_activated_nodes = 0;
    private static float cumulative_opinion = 0;

    private static MyNode best_candiate;
    private static Map<Double, Double> best_node_to_activate;
    private static Collection<MyNode> potential_candidates;
    private static SimpleDirectedWeightedGraph directedGraph;



    public static void main(String[] args) throws Exception {

        try {
            prop.load(new FileReader("config.properties"));
        }catch (Exception e){
            throw e;
        }

        seed_node = new ArrayList<>();


        //Read nodes
        Map<Double, MyNode> node_list = Read.readAllNodeWithOpinion(prop.getProperty("BASE_DIR") + prop.getProperty("DATA_2") + File.separator + "ids_with_opinion_small_2.txt");
        //Read edges
        Map<Double, List<MyEdge>> edges_list = Read.readAllEdgeWithInterest(prop.getProperty("BASE_DIR") + prop.getProperty("DATA_2") + File.separator + "edges_weighted_small_2.txt");

        inizializeGraph(node_list, edges_list);

        greedyAlg(node_list);
        //potentialMarginalAlg(node_list);
    }

    private static void inizializeGraph(Map<Double, MyNode> node_list, Map<Double, List<MyEdge>> edges_list){
        directedGraph = new SimpleDirectedWeightedGraph<MyNode, DefaultWeightedEdge>(DefaultWeightedEdge.class);

        //Add all node to the graph
        for (MyNode node_id : node_list.values()){
            directedGraph.addVertex(node_id);
        }

        //Add all edge to the graph
        for (double source_id: edges_list.keySet()){
            for (MyEdge my_edge : edges_list.get(source_id)) {
                try {
                    DefaultWeightedEdge graph_edge = (DefaultWeightedEdge) directedGraph.addEdge(node_list.get(my_edge.getSource_node()), node_list.get(my_edge.getTarget_node()));
                    directedGraph.setEdgeWeight(graph_edge, my_edge.getWeight());
                }catch (IllegalArgumentException ie){
                    System.err.println(ie);
                    System.err.println(my_edge.toString());
                }
            }
        }

        System.out.println("Graph created");
        System.out.println("Node of the graph: " + directedGraph.vertexSet().size());
        System.out.println("Edge of the graph: " + directedGraph.edgeSet().size());
    }

    private static void greedyAlg(Map<Double, MyNode> node_list){
            for (int i = 0; i < K; i++) {
            best_candiate = new MyNode();
            best_node_to_activate = new HashMap<>();
            double max_marginal_gain = 0D;

            System.out.println("\n\nturn: " + i + "\n\n");

            for (MyNode node : node_list.values()) {
                //System.out.print("candidate node: " + node.toString() + "\t");
                //System.out.println("-------------------BFS greedy search----------------");
                Map<Double, MyNodeUpdateInfo> nodes_to_activate = new HashMap<>();
                Gains gain = MyBFS.marginalGain(node, directedGraph, nodes_to_activate);
                System.out.println(node.getNode_id() + "\t" + gain.opinion_gain + "\t" + gain.positively_activated_gain);
            }
        }
    }


    private static void potentialMarginalAlg(Map<Double, MyNode> node_list){
        potential_candidates = null;
        for (int i = 0; i < K; i++){
            potential_candidates = CELF.potentialCandidateSelection((2^p)*K, directedGraph);

            for (MyNode node : potential_candidates){
                if (node.getNode_id() == 50213.0){
                    System.out.println(node.toStringComplete());
                }
            }

            best_candiate = new MyNode();
            best_node_to_activate= new HashMap<>();
            double max_marginal_gain = 0D;

            System.out.println("\n\nturn: " + i +"\n\n");

            for (MyNode node : potential_candidates){
                //System.out.print("candidate node: " + node.toString() + "\t");
                //System.out.println("-------------------BFS greedy search----------------");
                Map<Double, Double> nodes_to_activate = new HashMap<>();
                double node_marginal_gain = 0;//Greedy.marginalGain(node, directedGraph, nodes_to_activate);
                //System.out.println(node_marginal_gain + "\t" + max_marginal_gain);
                if (node_marginal_gain > max_marginal_gain){
                    //System.out.println("\nNew best candidate\t" + node.toString()+"\n");
                    best_candiate = node;
                    best_node_to_activate = nodes_to_activate;
                    max_marginal_gain = node_marginal_gain;
                    System.out.println("marginal gain: " + max_marginal_gain);
                }
                //System.out.println("-------------------------------------------------------");
            }

            if (best_node_to_activate.size() == 0){
                System.out.println("No more node to activate");
                break;
            }else {
                System.out.println("Best candiadte selected " + best_candiate.toString() + " at turn: " + i);
                seed_node.add(best_candiate);
                //positive_activated_nodes += Greedy.activateNode(best_node_to_activate, node_list);
                potential_candidates.remove(best_candiate);
                //results
                activated_nodes += best_node_to_activate.size();
                cumulative_opinion += max_marginal_gain;
            }
            System.out.println();
            System.out.println("Totally activated nodes: "  + activated_nodes);
            System.out.println("Positively activated nodes: "  + positive_activated_nodes);
            System.out.println("Totally opinion: "  + cumulative_opinion);
        }
    }

}
