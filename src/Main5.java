import Algorithem.CELF;
import Algorithem.InizializeGraph;
import Algorithem.MyBFS;
import Algorithem.UpdateStatus;
import DAO.*;
import Utils.Read;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

/**
 * Created by andompesta on 10/16/15.
 */
public class Main5 {
    private static Properties prop = new Properties();
    private static SimpleDirectedWeightedGraph directedGraph;
    private static final int K = 50;
    private static final int p = 4;

    private enum OBJ_FUNCTION { OPINION, POSITIVE_ACTIVATED_NODE }
    private enum BFS_FUNCTION {MYBFS, UPDATE_STATUS}

    private static OBJ_FUNCTION obj_function = OBJ_FUNCTION.POSITIVE_ACTIVATED_NODE;
    private static BFS_FUNCTION bfs = BFS_FUNCTION.MYBFS;

    private static GraphPerformance performance;
    private static List<MyNode> seed_nodes;


    private static String DATA_SET = "DATA_2";
    private static String log_name = "OVM-k="+K+"_pos_act.log";
    private static BufferedWriter LOG = null;


    public static void main(String[] args) throws Exception {
        try {
            prop.load(new FileReader("config.properties"));
        }catch (Exception e){
            throw e;
        }
        seed_nodes = new ArrayList<>();
        performance = new GraphPerformance();

        //Read nodes
        Map<Double, MyNode> node_list = Read.readAllNodeWithOpinion(prop.getProperty("BASE_DIR") + prop.getProperty(DATA_SET) + File.separator + "ids_with_opinion_threshold.txt");
        //Read edges
        Map<Double, List<MyEdge>> edges_list = Read.readAllEdgeWithInterest(prop.getProperty("BASE_DIR") + prop.getProperty(DATA_SET) + File.separator + "edges_weighted.txt");
        LOG = new BufferedWriter(new FileWriter(prop.getProperty("BASE_DIR") + prop.getProperty(DATA_SET) + File.separator + log_name));

        directedGraph = InizializeGraph.init(node_list, edges_list);


        //compute candidates
        List<MyNode> candicate_nodes = CELF.potentialCandidateSelection( (((2^p) * K)), directedGraph);

        PriorityQueue<MyNode> Q = new PriorityQueue<>(candicate_nodes.size(), new Comparator<MyNode>() {
            @Override
            public int compare(MyNode myNode, MyNode t1) {
                return Double.compare(t1.score, myNode.score);
            }
        });

        try {
            //compute marginal gain
            for (MyNode node : candicate_nodes) {
                Gains gain = null;
                switch (bfs) {
                    case MYBFS:
                        Map<Double, MyNodeUpdateInfo> nodes_to_activate = new HashMap<>();
                        gain = MyBFS.marginalGain(node, directedGraph, nodes_to_activate);
                        break;
                    case UPDATE_STATUS:
                        nodes_to_activate = UpdateStatus.updateActivationStatus(node, directedGraph);
                        gain = UpdateStatus.myUpdateOpinion(directedGraph, nodes_to_activate, node_list);
                        break;
                }

                switch (obj_function) {
                    case OPINION:
                        node.score = gain.opinion_gain;
                        break;
                    case POSITIVE_ACTIVATED_NODE:
                        node.score = gain.positively_activated_gain;
                        break;
                }

                Q.offer(node);
            }

            //Need to activate the status of first nodes
            int turn = 0;
            while (seed_nodes.size() < K) {
                MyNode u = null;
                System.out.println("\n\nturn: " + turn + "\n\n");
                turn++;
                Map<Double, MyNodeUpdateInfo> nodes_to_activate = null;
                Gains gain = null;
                do {
                    u = Q.poll();
                    /*
                    nodes_to_activate = UpdateStatus.updateActivationStatus(u, directedGraph);
                    gain = UpdateStatus.myUpdateOpinion(directedGraph, nodes_to_activate, node_list);
                    */

                    switch (bfs) {
                        case MYBFS:
                            nodes_to_activate = new HashMap<>();
                            gain = MyBFS.marginalGain(u, directedGraph, nodes_to_activate);
                            break;
                        case UPDATE_STATUS:
                            nodes_to_activate = UpdateStatus.updateActivationStatus(u, directedGraph);
                            gain = UpdateStatus.myUpdateOpinion(directedGraph, nodes_to_activate, node_list);
                            break;
                    }

                    switch (obj_function) {
                        case OPINION:
                            u.score = gain.opinion_gain;
                            break;
                        case POSITIVE_ACTIVATED_NODE:
                            u.score = gain.positively_activated_gain;
                            break;
                    }

                    if (gain.opinion_gain < Q.peek().score) {
                        Q.add(u);
                    }
                } while (Q.peek().score > u.score);

                if (u.score < 0) {
                    break;
                }
                seed_nodes.add(u);

                performance.positive_activated_nodes += gain.positively_activated_gain;
                performance.total_opinion += gain.opinion_gain;

                LOG.write(turn + "\t" + u.getNode_id() + "\t" + performance.total_opinion + "\t" + performance.positive_activated_nodes + "\t" + nodes_to_activate.size() + "\t" + gain.opinion_gain + "\t" + gain.positively_activated_gain + "\n");

                System.out.println(turn + "\t" + u.getNode_id() + "\t" + performance.total_opinion + "\t" + performance.positive_activated_nodes + "\t" + nodes_to_activate.size() + "\t" + gain.opinion_gain + "\t" + gain.positively_activated_gain + "\n");
                //activation of the nodes
                for (double node_id : nodes_to_activate.keySet()) {
                    MyNodeUpdateInfo user_info = nodes_to_activate.get(node_id);

                    MyNode node = node_list.get(node_id);
                    node.setOpinion(user_info.opinion);
                    node.setActivated(true);
                    node_list.put(node_id, node);
                }
            }
        }catch (Exception e){
            throw e;
        }finally {
            if (LOG != null)
                LOG.close();
        }
    }



}
