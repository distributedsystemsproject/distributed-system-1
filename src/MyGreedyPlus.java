import Algorithem.CELF;
import Algorithem.MyBFS;
import Algorithem.InizializeGraph;
import DAO.*;
import Utils.Read;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.io.*;
import java.util.*;

/**
 * Created by andompesta on 10/16/15.
 */
public class MyGreedyPlus {
    private static Properties prop = new Properties();
    private static SimpleDirectedWeightedGraph directedGraph;
    private static List<MyNode> seed_node;
    private static final int K = 50;
    private static final int p = 4;

    private enum OBJ_FUNCTION { OPINION, POSITIVE_ACTIVATED_NODE }
    private static OBJ_FUNCTION obj_function = OBJ_FUNCTION.POSITIVE_ACTIVATED_NODE;

    private static GraphPerformance performance;

    private static String DATA_SET = "DATA_3";
    private static String log_name = "CELF-k=50.log";
    private static BufferedWriter LOG = null;

    public static void main(String[] args) throws Exception {
        try {
            prop.load(new FileReader("config.properties"));

            seed_node = new ArrayList<>();
            performance = new GraphPerformance();

            //Read nodes
            Map<Double, MyNode> node_list = Read.readAllNodeWithOpinion(prop.getProperty("BASE_DIR") + prop.getProperty(DATA_SET) + File.separator + "ids_with_opinion_threshold.txt");
            //Read edges
            Map<Double, List<MyEdge>> edges_list = Read.readAllEdgeWithInterest(prop.getProperty("BASE_DIR") + prop.getProperty(DATA_SET) + File.separator + "edges_weighted.txt");
            LOG = new BufferedWriter(new FileWriter(prop.getProperty("BASE_DIR") + prop.getProperty(DATA_SET) + File.separator + log_name));


            directedGraph = InizializeGraph.init(node_list, edges_list);
            long start = System.nanoTime();
            start(obj_function, node_list);
            long end = System.nanoTime();
            long time = end - start;
            LOG.write(Long.toString(time)+"\n");
            System.out.println("FINISH");
        }catch (Exception e){
            throw e;
        }finally {
            if (LOG != null)
                LOG.close();
        }
        //potentialMarginalAlg(node_list);
    }

    private static void start(OBJ_FUNCTION function, Map<Double, MyNode> node_list) throws IOException {

        List<MyNode> candicate_nodes = CELF.potentialCandidateSelection((((2 ^ p) * K)), directedGraph);

        int i = 0;
        while ( seed_node.size() < K) {
            MyNode best_candiate = null;
            Map<Double, MyNodeUpdateInfo> best_candidate_node_to_activate = null;
            Gains max_gain = new Gains();

            System.out.println("\n\nturn: " + i + "\n\n");


            for (MyNode node : candicate_nodes) {
                //System.out.print("candidate node: " + node.toString() + "\t");
                //System.out.println("-------------------BFS greedy search----------------");
                Map<Double, MyNodeUpdateInfo> nodes_to_activate = new HashMap<>();
                Gains gain = MyBFS.marginalGain(node, directedGraph, nodes_to_activate);
                switch (function){
                    case OPINION:
                        if (gain.opinion_gain > max_gain.opinion_gain){
                            best_candiate = node;
                            best_candidate_node_to_activate = nodes_to_activate;
                            max_gain = gain;
                            //System.out.println("MAX OPINION:\t" + max_gain.opinion_gain);
                        }
                        break;
                    case POSITIVE_ACTIVATED_NODE:
                        if (gain.positively_activated_gain > max_gain.positively_activated_gain){
                            best_candiate = node;
                            best_candidate_node_to_activate = nodes_to_activate;
                            max_gain = gain;
                            //System.out.println("MAX POSITIVE_ACTIVATED_NODE:\t" + max_gain.positively_activated_gain);
                        }
                        break;
                }
            }

            if (best_candidate_node_to_activate.isEmpty()) {
                System.out.println("No more node to activate");
                break;
            } else {
                //System.out.println("Best candiadte selected " + best_candiate.toString() + " at turn: " + i);
                seed_node.add(best_candiate);
                MyBFS.activateNode(best_candidate_node_to_activate, node_list);
                //System.out.println(best_candidate_node_to_activate.size());
                candicate_nodes.remove(best_candiate);
                //System.err.println(candicate_nodes.size() + "\t" + best_candiate.getNode_id());
                performance.total_opinion += max_gain.opinion_gain;
                performance.positive_activated_nodes += max_gain.positively_activated_gain;
                //performance.totally_activated_nodes = total_active_node(directedGraph);
                LOG.write(i + "\t" + best_candiate.getNode_id() + "\t" + performance.total_opinion + "\t" + performance.positive_activated_nodes + "\t" + best_candidate_node_to_activate.size() + "\t" +max_gain.opinion_gain + "\t" + max_gain.positively_activated_gain + "\n");
                System.out.println(i + "\t" + best_candiate.getNode_id() + "\t" + performance.total_opinion + "\t" + performance.positive_activated_nodes + "\t" + best_candidate_node_to_activate.size() + "\t" +max_gain.opinion_gain + "\t" + max_gain.positively_activated_gain + "\n");
                /*
                for ( MyNode node : (Set<MyNode>) directedGraph.vertexSet()){
                    if (node.getNode_id() == best_candiate.getNode_id()) {
                        System.out.println(node.toStringComplete());
                        break;
                    }
                }
                */
            }
            i++;
            /*
            System.out.println();
            System.out.println("Totally activated nodes: " + performance.totally_activated_nodes);
            System.out.println("Positively activated nodes: "  + performance.positive_activated_nodes);
            System.out.println("Totally opinion: " + performance.total_opinion);
            */
        }
    }


    private static int total_active_node(SimpleDirectedWeightedGraph directedGraph){
        int ret = 0;
        int post_ret = 0;
        for (MyNode node : (Collection<MyNode>) directedGraph.vertexSet()){
            if (node.isActivated()) {
                ret++;
                if (node.getOpinion() > 0)
                    post_ret++;
            }
        }
        System.out.println("total_active_node: " + ret);
        System.out.println("positive_total_active_node: " + post_ret);
        return ret;
    }
}
