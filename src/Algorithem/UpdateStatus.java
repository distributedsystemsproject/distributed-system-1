package Algorithem;

import DAO.Gains;
import DAO.MyNode;
import DAO.MyNodeUpdateInfo;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.*;

/**
 * Created by andompesta on 10/14/15.
 */
public class UpdateStatus {

    public static Map<Double, MyNodeUpdateInfo> updateActivationStatus(MyNode source_node, SimpleDirectedWeightedGraph<MyNode, DefaultWeightedEdge> directedGraph) throws Exception {
        Map<Double , MyNodeUpdateInfo> A = new HashMap<>();
        //set of the activated nodes
        Deque<MyNode> Q = new ArrayDeque<>();

        if (!source_node.isActivated()) {
            A.put(source_node.getNode_id(), new MyNodeUpdateInfo(source_node.getNode_id(), source_node.getOpinion(), 0));
            Q.add(source_node);
        }

        while (!Q.isEmpty()){
            MyNode u = Q.poll();
            MyNodeUpdateInfo u_info = A.get(u.getNode_id());

            if (u_info == null)
                throw new Exception("1 - info not present: " + u.getNode_id());

            for (DefaultWeightedEdge u_out_edge : (Collection<DefaultWeightedEdge>) directedGraph.outgoingEdgesOf(u)){
                MyNode x = directedGraph.getEdgeTarget(u_out_edge);
                MyNodeUpdateInfo x_info = new MyNodeUpdateInfo(x.getNode_id(), x.getOpinion());
                if (A.containsKey(x.getNode_id())){
                    x_info = A.get(x.getNode_id());
                }

                if ( !A.containsKey(x.getNode_id()) && !x.isActivated()) {
                    double activation_influence = parentInfluence(x, directedGraph, A);
                    if (activation_influence >= x.getActivation_threshold()) {
                        x_info.activation_round = u_info.activation_round + 1;
                        A.put(x.getNode_id(), x_info);
                        Q.add(x);

                    }
                } else if ( x.isActivated() ) {
                    try {
                        x_info.activation_round = u_info.activation_round + 1;
                    }catch (NullPointerException np){
                        System.out.println(x_info + "\t" + u_info);
                    }

                    A.put(x.getNode_id(), x_info);
                }
            }
        }

        return A;
    }

    public static Gains myUpdateOpinion(SimpleDirectedWeightedGraph<MyNode, DefaultWeightedEdge> directedGraph, Map<Double, MyNodeUpdateInfo> A, Map<Double, MyNode> node_list) throws Exception {
        Gains ret = new Gains();

        List<MyNodeUpdateInfo> users_status = new ArrayList<MyNodeUpdateInfo>(A.values());
        Collections.sort(users_status, new Comparator<MyNodeUpdateInfo>() {
            @Override
            public int compare(MyNodeUpdateInfo o1, MyNodeUpdateInfo o2) {
                return Integer.compare(o1.activation_round, o2.activation_round);
            }
        });


        for (MyNodeUpdateInfo u_info : users_status){
            double new_opinion = 0D;
            MyNode u = node_list.get(u_info.node_id);

            if (u_info.activation_round == 0){
                new_opinion = u.getOpinion();
            }else {
                for (DefaultWeightedEdge u_in_edge : (Collection<DefaultWeightedEdge>) directedGraph.incomingEdgesOf(node_list.get(u_info.node_id))) {
                    MyNode x = directedGraph.getEdgeSource(u_in_edge);

                    if (!A.containsKey(x.getNode_id()))
                        continue;
                        //throw new Exception("2 - info not present: " + u.getNode_id());

                    MyNodeUpdateInfo x_info = A.get(x.getNode_id());

                    if (x.isActivated() && x_info.activation_round < u_info.activation_round) {
                        new_opinion += x.getOpinion() * directedGraph.getEdgeWeight(u_in_edge);
                    } else if (A.containsKey(x.getNode_id()) && x_info.activation_round < u_info.activation_round) {
                        new_opinion += A.get(x.getNode_id()).opinion * directedGraph.getEdgeWeight(u_in_edge);
                    }
                }
                new_opinion = checkSaturation(u.getOpinion(), new_opinion);
            }
            if (new_opinion > 0)
                ret.positively_activated_gain++;
            u.setOpinion(new_opinion);
            ret.opinion_gain += new_opinion;
        }
        //System.out.println(cumulative_opinion + "\t" + positive_users + "\t" + A.size());
        return ret;
    }

    private static double checkSaturation(double opinion, double influence){
        if (opinion + influence > 1){
            return 1;
        } else if (opinion + influence < -1){
            return -1;
        }else {
            return opinion + influence;
        }
    }


    private static double parentInfluence(MyNode node, SimpleDirectedWeightedGraph graph, Map<Double, MyNodeUpdateInfo> nodes_to_activate){
        double activation_influence = 0D;

        for (DefaultWeightedEdge x_in_edge : (Set<DefaultWeightedEdge>) graph.incomingEdgesOf(node)){
            MyNode parent_node = (MyNode) graph.getEdgeSource(x_in_edge);
            if (parent_node.isActivated()){
                activation_influence += graph.getEdgeWeight(x_in_edge);
            }
            else if ( nodes_to_activate.containsKey(parent_node.getNode_id())){
                activation_influence += graph.getEdgeWeight(x_in_edge);
            }
        }

        return activation_influence;
    }
}
