package Algorithem;

import DAO.Gains;
import DAO.MyNode;
import DAO.MyNodeUpdateInfo;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.*;

/**
 * Created by ando on 10/12/15.
 */
public class MyBFS {

    public static Gains marginalGain(MyNode source_node, SimpleDirectedWeightedGraph<MyNode, DefaultWeightedEdge> directedGraph,
                                      Map<Double, MyNodeUpdateInfo> nodes_to_activate){
        Gains ret = new Gains();

        if (!source_node.isActivated()) {
            nodes_to_activate.put(source_node.getNode_id(), new MyNodeUpdateInfo(source_node.getNode_id(), source_node.getOpinion(), 0) );
            ret.opinion_gain += source_node.getOpinion();
            if (source_node.getOpinion() > 0)
                ret.positively_activated_gain++;

        }

        IteratorBFS iterator = new IteratorBFS(source_node, directedGraph);

        //remove the source_node itself
        iterator.next();
        iterator.queueChild(source_node, nodes_to_activate);

        while (iterator.hasNext()){
            MyNode u_node = iterator.next();

            //do not modify active node
            if ( !u_node.isActivated() && !nodes_to_activate.containsKey(u_node.getNode_id()) ) {
                double activation_threshold = u_node.getActivation_threshold();
                double parent_influence = 0D;
                double activation_influence = 0D;

                //activation function
                for (DefaultWeightedEdge incoming_edge : (Set<DefaultWeightedEdge>) directedGraph.incomingEdgesOf(u_node)) {
                    MyNode parent = directedGraph.getEdgeSource(incoming_edge);
                    if (parent.isActivated()) {
                        parent_influence += parent.getOpinion() * directedGraph.getEdgeWeight(incoming_edge);
                        activation_influence += directedGraph.getEdgeWeight(incoming_edge);
                    } else if(nodes_to_activate.containsKey(parent.getNode_id())){
                        parent_influence += nodes_to_activate.get(parent.getNode_id()).opinion * directedGraph.getEdgeWeight(incoming_edge);
                        activation_influence += directedGraph.getEdgeWeight(incoming_edge);
                    }
                }

                //obj function update
                if (activation_influence >= activation_threshold) {
                    double new_opinion = checkSaturation(u_node.getOpinion(), parent_influence);
                    nodes_to_activate.put(u_node.getNode_id(), new MyNodeUpdateInfo(u_node.getNode_id(), new_opinion));
                    ret.opinion_gain += new_opinion;
                    if (new_opinion > 0)
                        ret.positively_activated_gain++;
//                    System.out.println("parent: " + iterator.seenParent(child_node.getNode_id()) + "\t" + child_node.getNode_id());
//                    for (Double node_id:nodes_to_activate.keySet()){
//                        System.out.println(node_id);
//                    }
                    //System.out.println("gain:"  +gain);
                }
            }
            if (u_node.isActivated() || nodes_to_activate.containsKey(u_node.getNode_id()))
                iterator.queueChild(u_node, nodes_to_activate);
        }
        return ret;
    }


    public static int activateNode(Map<Double, MyNodeUpdateInfo> nodes_to_activate, Map<Double, MyNode> node_list){
        int ret = 0;
        for (double node_to_activate_id: nodes_to_activate.keySet()){
            MyNode node_to_activate = node_list.get(node_to_activate_id);
            node_to_activate.setActivated(true);
            node_to_activate.setOpinion(nodes_to_activate.get(node_to_activate_id).opinion);
            if (node_to_activate.getOpinion() > 0)
                ret += 1;
            //System.out.print(node_to_activate.toString() + " ");
            /*
            System.out.println(node_to_activate.toStringComplete());
            System.out.println();
            for (MyNode graph_node : directedGraph.vertexSet()){
                if (graph_node.getNode_id() == node_to_activate_id) {
                    System.out.println(graph_node.toStringComplete());
                    System.out.println("--------------------");
                }
            }
            */
        }
        //System.out.println();
        return ret;
    }

    private static double checkSaturation(double opinion, double influence){
        if (opinion + influence > 1){
            return 1;
        } else if (opinion + influence < -1){
            return -1;
        }else {
            return opinion + influence;
        }
    }


    private static double parentInfluence(MyNode node, SimpleDirectedWeightedGraph graph, Map<Double, MyNodeUpdateInfo> nodes_to_activate){
        double activation_influence = 0D;

        for (DefaultWeightedEdge x_in_edge : (Set<DefaultWeightedEdge>) graph.incomingEdgesOf(node)){
            MyNode parent_node = (MyNode) graph.getEdgeSource(x_in_edge);
            if (parent_node.isActivated()){
                activation_influence += graph.getEdgeWeight(x_in_edge);
            }
            else if ( nodes_to_activate.containsKey(parent_node.getNode_id())){
                activation_influence += graph.getEdgeWeight(x_in_edge);
            }
        }

        return activation_influence;
    }
}
