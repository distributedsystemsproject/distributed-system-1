package Algorithem;

import DAO.MyNode;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.*;

/**
 * Created by ando on 10/19/15.
 */
public class PageRank {

    private static int max_iter_number = 200;
    public static class PRScore{
        public double id;
        public double pr_score;

        public PRScore() {
        }

        public PRScore(double id, double pr_score) {
            this.id = id;
            this.pr_score = pr_score;
        }
    }

    public static Collection<PRScore> calculatePageRank(double dampind_factor, SimpleDirectedWeightedGraph graph)
    {
        int num_iter = 0;
        // Create new/old PageRank vectors for iteration
        Map<Double, PRScore> PR = new HashMap<Double, PRScore>();
        Map<Double, PRScore> PR_new = new HashMap<Double, PRScore>();

        //Inizialize
        for (MyNode node : (Set<MyNode>)graph.vertexSet()){
            PR.put(node.getNode_id(), new PRScore(node.getNode_id(), node.getOpinion()) );
        }

        // PageRank!!!
        double change;
        do {
            for(MyNode u_node : (Set<MyNode>) graph.vertexSet()) {
                if (graph.incomingEdgesOf(u_node).isEmpty()){
                    PR.remove(u_node.getNode_id());
                }else {
                    for (DefaultWeightedEdge in_edge : (Set<DefaultWeightedEdge>) graph.incomingEdgesOf(u_node)) {
                        double edge_weight = graph.getEdgeWeight(in_edge);
                        /*
                        MyNode v_node = (MyNode) graph.getEdgeSource(in_edge);

                        if (PR_new.containsKey(u_node.getNode_id())) {
                            double score = PR_new.get(u_node.getNode_id()).pr_score + PR.get(u_node.getNode_id()).pr_score * (edge_weight * v_node.getOpinion());
                            PR_new.put(u_node.getNode_id(), new PRScore(u_node.getNode_id(), score));
                        } else {
                            double score = PR.get(u_node.getNode_id()).pr_score * (edge_weight * v_node.getOpinion());
                            PR_new.put(u_node.getNode_id(), new PRScore(u_node.getNode_id(), score));
                        }
                        */

                        //update opinion
                        //u_node.setOpinion(u_node.getOpinion() + (edge_weight * v_node.getOpinion()));

                        if (PR_new.containsKey(u_node.getNode_id())) {
                            double score = PR_new.get(u_node.getNode_id()).pr_score + (PR.get(u_node.getNode_id()).pr_score * edge_weight);
                            PR_new.put(u_node.getNode_id(), new PRScore(u_node.getNode_id(), score));
                        } else {
                            double score = PR.get(u_node.getNode_id()).pr_score * edge_weight;
                            PR_new.put(u_node.getNode_id(), new PRScore(u_node.getNode_id(), score));
                        }
                    }
                }
            }//end: for(i)

            //double mag = 0;
            for(Map.Entry<Double, PRScore> PR_new_entity: PR_new.entrySet()) {
                PRScore node_score = PR_new_entity.getValue();
                node_score.pr_score = (1-dampind_factor) + ( dampind_factor * node_score.pr_score);
                PR_new.put(PR_new_entity.getKey(), node_score);
                //mag += node_score.pr_score;
            }//end: for(x)

            // Calculate change between PR generations
            change = pageRankDiff(PR, PR_new);
            // Reset new PR array.
            for(double key : PR.keySet()) {
                PRScore node_score = PR_new.get(key);
                //node_score.pr_score = node_score.pr_score/mag;
                //PR_new.put(key, node_score);
                PR.put(key, new PRScore(node_score.id, node_score.pr_score) );
            }
            PR_new.clear();
            num_iter++;
        }while(change < 0.001 || num_iter < max_iter_number );

        return PR.values();
    }

    protected static double pageRankDiff(Map<Double, PRScore> oldPR, Map<Double, PRScore> newPR)
    {
        float diff = 0;
        double key = 0D;
        try {
            for (double temp_key : oldPR.keySet() ) {
                key = temp_key;
                PRScore pr_score = oldPR.get(key);
                PRScore pr_new_scpre = newPR.get(key);
                diff += Math.abs(pr_score.pr_score - pr_new_scpre.pr_score);
            }//end: for(x)
        }catch (NullPointerException ne){
            System.out.println(key);
            throw ne;
        }


        System.out.println("Difference: " + diff);
        return diff;
    }//end: pageRankDiff(double[], double[])



}
