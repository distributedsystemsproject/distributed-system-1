package Algorithem;

import DAO.MyEdge;
import DAO.MyNode;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.List;
import java.util.Map;

/**
 * Created by andompesta on 10/16/15.
 */
public class InizializeGraph {
    public static SimpleDirectedWeightedGraph init(Map<Double, MyNode> node_list, Map<Double, List<MyEdge>> edges_list){
        SimpleDirectedWeightedGraph directedGraph = new SimpleDirectedWeightedGraph<MyNode, DefaultWeightedEdge>(DefaultWeightedEdge.class);

        //Add all node to the graph
        for (MyNode node_id : node_list.values()){
            directedGraph.addVertex(node_id);
        }

        //Add all edge to the graph
        for (double source_id: edges_list.keySet()){
            for (MyEdge my_edge : edges_list.get(source_id)) {
                try {
                    DefaultWeightedEdge graph_edge = (DefaultWeightedEdge) directedGraph.addEdge(node_list.get(my_edge.getSource_node()), node_list.get(my_edge.getTarget_node()));
                    directedGraph.setEdgeWeight(graph_edge, my_edge.getWeight());
                }catch (IllegalArgumentException ie){
                    System.err.println(ie);
                    System.err.println(my_edge.toString());
                }
            }
        }

        System.out.println("Graph created");
        System.out.println("Node of the graph: " + directedGraph.vertexSet().size());
        System.out.println("Edge of the graph: " + directedGraph.edgeSet().size());
        return directedGraph;
    }
}
