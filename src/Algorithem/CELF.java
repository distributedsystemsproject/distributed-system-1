package Algorithem;

import DAO.MyNode;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.*;

/**
 * Created by ando on 10/13/15.
 */
public class CELF {

    public static List<MyNode> potentialCandidateSelection(int queue_size, SimpleDirectedWeightedGraph graph){
        List<MyNode> priority_queue = new ArrayList<>();

        for (MyNode node : (Set<MyNode>) graph.vertexSet()){
            double p_marginal = potentialMargial(node, graph);
            node.score = p_marginal;
            priority_queue.add(node);
        }

        Collections.sort(priority_queue, new Comparator<MyNode>() {
            @Override
            public int compare(MyNode o1, MyNode o2) {
                if (o1.score >= o2.score)
                    return -1;
                else
                    return 1;
            }

            @Override
            public boolean equals(Object obj) {
                MyNode node = (MyNode) obj;
                return this.equals(node);
            }
        });

        priority_queue = priority_queue.subList(0, queue_size);
        return priority_queue;
    }


    private static double potentialMargial(MyNode source_node, SimpleDirectedWeightedGraph graph){
        double potential_marginal = source_node.getOpinion();

        for (DefaultWeightedEdge edge : (Collection<DefaultWeightedEdge>) graph.outgoingEdgesOf(source_node)){
            MyNode target_node = (MyNode) graph.getEdgeTarget(edge);
            if (target_node.isActivated())
                potential_marginal += target_node.getOpinion() + (source_node.getOpinion() * graph.getEdgeWeight(edge));
            else {
                double edge_weight = graph.getEdgeWeight(edge);
                potential_marginal += (edge_weight/target_node.getActivation_threshold()) *
                        (target_node.getOpinion() + (source_node.getOpinion() * edge_weight));
            }
        }

        return potential_marginal;
    }
}
