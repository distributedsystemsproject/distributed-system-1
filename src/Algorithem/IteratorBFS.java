package Algorithem;

import DAO.MyNode;
import DAO.MyNodeUpdateInfo;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.*;


/**
 * Created by ando on 10/12/15.
 */
public class IteratorBFS {
    private Deque<MyNode> queue;
    //Contains the id of the seen nodes and the parents that call it
    private Map<Double, Double> seen;

    private SimpleDirectedWeightedGraph graph;


    public IteratorBFS(MyNode starting_node, SimpleDirectedWeightedGraph graph) {
        this.graph = graph;
        this.queue = new ArrayDeque<>();
        this.queue.add(starting_node);
        this.seen = new HashMap<>();
        this.seen.put(starting_node.getNode_id(), null);
    }

    public MyNode next(){
        return this.queue.removeFirst();
    }

    public void queueChild(MyNode activated_node, Map<Double, MyNodeUpdateInfo> nodes_to_activate){
        //System.out.print("Look for child of: " +activated_node.toString() + "\t\t");
        for (DefaultWeightedEdge edge : (Collection<DefaultWeightedEdge>) graph.outgoingEdgesOf(activated_node)) {
            MyNode child_vertex = (MyNode) graph.getEdgeTarget(edge);

            boolean is_active = false;
            if (child_vertex.isActivated() || nodes_to_activate.containsKey(child_vertex.getNode_id())) {
                is_active = true;
            }

            if (!this.seen.containsKey(child_vertex.getNode_id())) {
                this.queue.add(child_vertex);
                //System.out.print("child: " + child_vertex.toString() + " ");
                this.seen.put(child_vertex.getNode_id(), activated_node.getNode_id());
            }
            //else if ( !is_active && this.seen.get(activated_node.getNode_id()) != child_vertex.getNode_id() ){
            else if (!is_active){
                Iterator<MyNode> it = this.queue.iterator();
                while (it.hasNext()){
                    if (it.next() == child_vertex)
                        it.remove();
                }
                //update parent form which I arrive at this node
                this.queue.addFirst(child_vertex);
                this.seen.put(child_vertex.getNode_id(), activated_node.getNode_id());
            }
        }
    }

    public boolean hasNext(){
        return !this.queue.isEmpty();
    }

}
