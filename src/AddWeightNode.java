import DAO.MyNode;
import Utils.RandomUtils;
import Utils.Read;
import Utils.Write;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by ando on 10/9/15.
 */
public class AddWeightNode {

    private static Properties prop = new Properties();


    public static void main(String[] args) throws Exception {
        try {
            prop.load(new FileReader("config.properties"));
        }catch (Exception e){
            throw e;
        }

        //Read nodes
        List<Double> node_list = (List<Double>) Read.readAllNode(prop.getProperty("BASE_DIR") + prop.getProperty("DATA_2") + File.separator + "ids.txt");
        List<MyNode> node_list_opinion = new ArrayList<MyNode>();
        for (double node_id : node_list){
            node_list_opinion.add(new MyNode(node_id, RandomUtils.opinionGenerator()));
        }

        Write.writeNodesOpinion(prop.getProperty("BASE_DIR") + prop.getProperty("DATA_2") + File.separator + "ids_with_opinion.txt", node_list_opinion);


    }
}
