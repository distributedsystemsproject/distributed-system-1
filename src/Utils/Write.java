package Utils;

import DAO.MyEdge;
import DAO.MyNode;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by ando on 10/9/15.
 */
public class Write {

    public static void writeNodesOpinion(String path, List<MyNode> nodes_opinion_list) throws IOException {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(path));
            for (MyNode node: nodes_opinion_list) {
                writer.write(node.getNode_id() + " " + node.getOpinion()+ "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null)
                writer.close();
        }
    }

    public static void writeEdgesWeight(String path,  Map<Double, List<MyEdge>> source_edge_weight) throws IOException {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(path));
            for (double target_node_id : source_edge_weight.keySet()) {

                String source_node_str = "";
                for (MyEdge source_node : source_edge_weight.get(target_node_id)){
                    source_node_str += source_node.getSource_node() + "/" + source_node.getWeight() + " ";
                }
                source_node_str = source_node_str.trim();
                writer.write(target_node_id + "\t" + source_node_str+ "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null)
                writer.close();
        }
    }
}
