package Utils;

import java.util.Random;

/**
 * Created by ando on 10/9/15.
 */
public class RandomUtils {

    private static Random randomGenerator = new Random();

    public static float opinionGenerator(){
        float opinion = 0F;
        //(maxX - minX) * rand.nextFloat() + minX;
        opinion = 2 * randomGenerator.nextFloat() - 1;
        return opinion;
    }

    public static double edgeWeightGenerator(){
        double opinion = 0F;
        opinion = randomGenerator.nextDouble();
        return opinion;
    }
}
