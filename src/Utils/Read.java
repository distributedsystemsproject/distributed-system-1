package Utils;

import DAO.MyEdge;
import DAO.MyNode;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

/**
 * Created by ando on 10/8/15.
 */
public class Read {

    public static Collection<Double> readAllNode(String path) throws Exception {
        List<Double> ret = null;
        BufferedReader br = null;
        try{
            br = new BufferedReader(new FileReader(path));
            String line;
            ret = new ArrayList<Double>();
            while ((line = br.readLine()) != null){
                double value = Double.parseDouble(line);
                ret.add(value);
            }
        } catch (Exception e){
            throw e;
        } finally {
            if (br != null)
                br.close();
        }
        return ret;
    }


    public static Map<Double, MyNode> readAllNodeWithOpinion(String path) throws Exception {
        Map<Double, MyNode> ret = null;
        BufferedReader br = null;
        try{
            br = new BufferedReader(new FileReader(path));
            String line;
            ret = new HashMap<Double, MyNode>();
            while ((line = br.readLine()) != null){
                String[] tokens = line.split(" ");
                double node_id = Double.parseDouble(tokens[0]);
                double node_opinion = Double.parseDouble(tokens[1]);
                double node_threshold = Double.parseDouble(tokens[2]);
                ret.put(node_id, new MyNode(node_id, node_opinion, node_threshold));
            }
        } catch (Exception e){
            throw e;
        } finally {
            if (br != null)
                br.close();
        }
        return ret;
    }

    public static Map<Double, List<Double>> readAllEdge(String path) throws Exception {
        Map<Double, List<Double>> ret = null;
        BufferedReader br = null;
        try{
            br = new BufferedReader(new FileReader(path));
            String line;
            ret = new HashMap<Double, List<Double>>();

            while ((line = br.readLine()) != null){
                String[] tokens = line.split(" ");
                double id_node = Double.parseDouble(tokens[0]);
                double edge_node = Double.parseDouble(tokens[1]);

                if (id_node == edge_node) {
                    System.err.println(id_node + " " + edge_node);
                    continue;
                }

                if (ret.containsKey(id_node)){
                    List<Double> edges_node = ret.get(id_node);
                    edges_node.add(edge_node);
                    ret.put(id_node, edges_node);
                }else{
                    List<Double> edges_node = new ArrayList<Double>();
                    edges_node.add(edge_node);
                    ret.put(id_node, edges_node);
                }
            }
        } catch (Exception e){
            throw e;
        } finally {
            if (br != null)
                br.close();
        }
        return ret;
    }


    public static Map<Double, List<MyEdge>> readAllEdgeWithInterest(String path) throws Exception {
        Map<Double, List<MyEdge>> ret = null;

        Scanner file_reader = null;
        try{
            file_reader = new Scanner(new File(path));
            ret = new HashMap<Double, List<MyEdge>>();

            while (file_reader.hasNextLine()) {
                String line = file_reader.nextLine();
                String[] tokens = line.split("\t");
                if (tokens.length != 3)
                    throw new Exception("Error in format of the line: "+ line);

                double source_node = Double.parseDouble(tokens[0]);
                double target_node = Double.parseDouble(tokens[1]);
                double weight = Double.parseDouble(tokens[2]);
                MyEdge edge = new MyEdge(source_node, target_node, weight);

                List<MyEdge> edges = null;

                if (ret.containsKey(source_node))
                    edges = ret.get(source_node);
                else
                    edges = new ArrayList<MyEdge>();

                edges.add(edge);
                ret.put(source_node, edges);
            }
        } catch (Exception e){
            throw e;
        } finally {
            if (file_reader != null)
                file_reader.close();
        }
        return ret;
    }
}
