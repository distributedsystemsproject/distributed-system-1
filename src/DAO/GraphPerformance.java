package DAO;

/**
 * Created by andompesta on 10/16/15.
 */
public class GraphPerformance {
    public int totally_activated_nodes;
    public int positive_activated_nodes;
    public float total_opinion;

    public GraphPerformance() {
    }

    public GraphPerformance(int totally_activated_nodes, int positive_activated_nodes, float total_opinion) {
        this.totally_activated_nodes = totally_activated_nodes;
        this.positive_activated_nodes = positive_activated_nodes;
        this.total_opinion = total_opinion;
    }


}
