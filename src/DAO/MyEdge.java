package DAO;

/**
 * Created by ando on 10/9/15.
 */
public class MyEdge {
    private double source_node;
    private double target_node;
    private double weight;

    public MyEdge(double source_node, double target_node, double weight) {
        this.source_node = source_node;
        this.target_node = target_node;
        this.weight = weight;
    }

    public MyEdge() {
    }

    public MyEdge(double weight, double source_node) {
        this.weight = weight;
        this.source_node = source_node;
    }

    public double getSource_node() {
        return source_node;
    }

    public void setSource_node(double source_node) {
        this.source_node = source_node;
    }

    public double getTarget_node() {
        return target_node;
    }

    public void setTarget_node(double target_node) {
        this.target_node = target_node;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyEdge myEdge = (MyEdge) o;

        if (Double.compare(myEdge.source_node, source_node) != 0) return false;
        return Double.compare(myEdge.target_node, target_node) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(source_node);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(target_node);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "source_node=" + source_node + " target_node=" + target_node + " weight=" + weight;
    }
}
