package DAO;

/**
 * Created by andompesta on 10/16/15.
 */
public class Gains {
    public double opinion_gain;
    public int positively_activated_gain;

    public Gains() {
    }

    public Gains(double opinion_gain, int positively_activated_gain) {
        this.opinion_gain = opinion_gain;
        this.positively_activated_gain = positively_activated_gain;
    }

}
