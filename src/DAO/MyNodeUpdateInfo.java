package DAO;

/**
 * Created by andompesta on 10/16/15.
 */
public class MyNodeUpdateInfo {
    public double node_id;
    public double opinion;
    public int activation_round = -1;


    public MyNodeUpdateInfo() {
    }

    public MyNodeUpdateInfo(double node_id, double new_opinion) {
        this.node_id = node_id;
        this.opinion = new_opinion;
    }

    public MyNodeUpdateInfo(double node_id, double opinion, int activation_round) {
        this.node_id = node_id;
        this.activation_round = activation_round;
        this.opinion = opinion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyNodeUpdateInfo that = (MyNodeUpdateInfo) o;

        return Double.compare(that.node_id, node_id) == 0;

    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(node_id);
        return (int) (temp ^ (temp >>> 32));
    }
}
