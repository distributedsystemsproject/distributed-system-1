package DAO;

/**
 * Created by ando on 10/9/15.
 */
public class MyNode{
    private double node_id;
    private double opinion;
    private double activation_threshold;
    private boolean activated = false;

    public double score;
    //public int activation_round;


    public MyNode(double node_id, double opinion, double activation_threshold, double score) {
        this.node_id = node_id;
        this.opinion = opinion;
        this.activation_threshold = activation_threshold;
        this.score = score;
        //this.activation_round = -1;
    }

    public MyNode(double node_id, double opinion, double activation_threshold) {
        this.node_id = node_id;
        this.opinion = opinion;
        this.activation_threshold = activation_threshold;
        //this.activation_round = -1;
    }

    public MyNode(double node_id, double opinion) {
        this.node_id = node_id;
        this.opinion = opinion;
        //this.activation_round = -1;
    }

    public MyNode(double node_id) {
        this.node_id = node_id;
    }

    public MyNode() {
    }

    public double getNode_id() {
        return node_id;
    }

    public void setNode_id(double node_id) {
        this.node_id = node_id;
    }

    public double getOpinion() {
        return opinion;
    }

    public void setOpinion(double opinion) {
        this.opinion = opinion;
    }


    @Override
    public String toString() {
        return "node_id:" + node_id + " "+activated;
    }

    public String toStringComplete() {
        return "-----------------------------\n" + "node_id:" + node_id + '\n' +
                "opinion:" + opinion + '\n' +
                "activated:" + activated + '\n' +
                "activation_threshold:" + activation_threshold + "\n"+
                "potential_marginal_gain:" + score +
                "\n-----------------------------";

    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyNode myNode = (MyNode) o;

        if (Double.compare(myNode.node_id, node_id) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(node_id);
        return (int) (temp ^ (temp >>> 32));
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public double getActivation_threshold() {
        return activation_threshold;
    }

    public void setActivation_threshold(double activation_threshold) {
        this.activation_threshold = activation_threshold;
    }
}
